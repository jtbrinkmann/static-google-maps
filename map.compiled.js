{"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<!DOCTYPE html>\r\n\r\n<html lang=\"en\">\r\n\r\n<body>\r\n    <form id=\"searchbar\">\r\n        <input type=\"text\" id=\"search_input\" name=\"query\" />\r\n        <button type=\"submit\" id=\"search_btn\"></button>\r\n    </form>\r\n\r\n    <aside id=\"sidebar\">\r\n        Placeholder\r\n    </aside>\r\n\r\n    <div id=\"mapview\">\r\n        <img src=\""
    + alias4(((helper = (helper = helpers.mapview_url || (depth0 != null ? depth0.mapview_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"mapview_url","hash":{},"data":data,"loc":{"start":{"line":16,"column":18},"end":{"line":16,"column":33}}}) : helper)))
    + "\" />\r\n        <a id=\"link_nw\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":17,"column":30},"end":{"line":17,"column":42}}}) : helper)))
    + "&dir=nw\"></a>\r\n        <a id=\"link_n\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":18,"column":29},"end":{"line":18,"column":41}}}) : helper)))
    + "&dir=n\"></a>\r\n        <a id=\"link_ne\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":19,"column":30},"end":{"line":19,"column":42}}}) : helper)))
    + "&dir=ne\"></a>\r\n        <a id=\"link_w\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":20,"column":29},"end":{"line":20,"column":41}}}) : helper)))
    + "&dir=w\"></a>\r\n        <div id=\"link_placeholder\"></div>\r\n        <a id=\"link_e\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":22,"column":29},"end":{"line":22,"column":41}}}) : helper)))
    + "&dir=e\"></a>\r\n        <a id=\"link_sw\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":23,"column":30},"end":{"line":23,"column":42}}}) : helper)))
    + "&dir=sw\"></a>\r\n        <a id=\"link_s\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":24,"column":29},"end":{"line":24,"column":41}}}) : helper)))
    + "&dir=s\"></a>\r\n        <a id=\"link_se\" href=\""
    + alias4(((helper = (helper = helpers.link_url || (depth0 != null ? depth0.link_url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link_url","hash":{},"data":data,"loc":{"start":{"line":25,"column":30},"end":{"line":25,"column":42}}}) : helper)))
    + "&dir=se\"></a>\r\n    </div>\r\n</body>\r\n\r\n</html>";
},"useData":true}
