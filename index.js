const Handlebars = require("handlebars")
const fs = require("fs")
const express = require("express")
const appendQuery = require("append-query")
const fetch = require("node-fetch")

require("dotenv").config()

const { GMAPS_API_KEY, PORT } = process.env

const mapTemplate = Handlebars.compile(fs.readFileSync("./map.hbs").toString())

express()
  .get("/maps/api/staticmap", async (req, res) => {
    const forwardedRes = await fetch(`https://maps.googleapis.com/${req.url}`, {
      headers: {
        referrer:
          "https://developers.google.com/maps/documentation/maps-static/dev-guide",
      },
    })
    res.status(forwardedRes.status)
    forwardedRes.body.pipe(res)
  })
  .get("/", (req, res) => {
    res.redirect(
      appendQuery("/map", {
        lat: 54.3539482,
        long: 13.0700722,
        maptype: "roadmap",
        zoom: 15,
      })
    )
  })
  .get("/map", (req, res) => {
    const { maptype, zoom } = req.query
    lat = parseFloat(req.query.lat)
    long = parseFloat(req.query.long)

    data = {
      mapview_url: appendQuery("/maps/api/staticmap", {
        zoom,
        maptype,
        center: lat + "," + long,
        scale: 1,
        size: "1920x1080",
        format: "png",
        key: GMAPS_API_KEY,
      }),
    }

    const STEP_SIZE = 0.01
    const directions = {
      nw: [-1, -1],
      n: [0, -1],
      ne: [+1, -1],
      w: [-1, 0],
      e: [+1, 0],
      sw: [-1, +1],
      s: [0, +1],
      se: [+1, +1],
    }
    for (const direction in directions) {
      const [offsetY, offsetX] = directions[direction]
      data[`nav_url_${direction}`] = appendQuery("", {
        zoom,
        maptype,
        lat: lat + offsetX * -STEP_SIZE,
        long: long + offsetY * STEP_SIZE,
      })
    }

    res.send(mapTemplate(data))
  })

  .listen(PORT, () => console.log(`Example app listening on port ${PORT}!`))
